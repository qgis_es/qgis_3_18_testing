# QGIS 3.18 Testing

Recursos e información de las pruebas realizadas por miembros de la Asociación QGIS España para QGIS 3.18.0

## Objetivo

Actividad organizada la Asociación QGIS España probar y documentar las características de la version QGIS 3.18.0 Zürich (2021-02-19)

El listado de novedades se puede consultar en el siguiente enlace https://qgis.org/en/site/forusers/visualchangelog318/index.html

## Participantes
- Juanma Martín Linkedin -  Twitter - Web
- José Ruiz Manzanares [Linkedin](https://www.linkedin.com/in/jos%C3%A9-ruiz-manzanares-72854a42/) -  [Twitter](https://twitter.com/JoseRuizMan) - Web
- Emilio Sanchez [Linkedin](https://www.linkedin.com/in/emilio-s%C3%A1nchez-porcuna-1752a29a/) -  [Twitter](https://twitter.com/EmilioSPorcuna) - Web
- Josep Lluís Sala [Linkedin](https://www.linkedin.com/in/joseplluissala/) -  [Twitter](https://twitter.com/jllsala) - Web
- David García Hernández [Linkedin](https://www.linkedin.com/in/david-garc%C3%ADa-hern%C3%A1ndez-9336a915/) -  Twitter - Web
- Lucho Ferrer Linkedin -  Twitter - Web
- Patricio Soriano Castro [Linkedin](https://www.linkedin.com/in/patriciosorianocastro/) -  [Twitter](https://twitter.com/sigdeletras) - [Web](http://sigdeletras.com/)

## Características

### 01 User Interface - Juanma Martín

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#user-interface)

- Feature: Hide derived attributes from the Identify results
- Feature: Close all tabs at once from message logs interface
- Feature: API for layer source widgets
- Feature: GUI for dynamic SVGs
- Feature: Zoom and pan to selection for multiple layers
- Feature: Zoom in/out by scrolling mouse wheel over map overview panel

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/01](/recursos/01)

![GUI for dynamic SVGs](/recursos/01/GUI_for_dynamic_SVGs.mp4)

### 02 Accessibility - Patricio Soriano

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#accessibility)

- Feature: Improved color vision deficiency simulation	
- Feature: Rotation widget for the Georeferencer

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/01](/recursos/02)

![Rotation widget for the Georeferencer](/recursos/02/Georeferencer.mp4)

### 03 Symbology - José Ruiz Manzanares

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#symbology)

- Feature: Data defined overall symbol opacity
- Feature: Open the style gallery from the style manager

**Comentarios**

*Datos Definidos sobre símbolos opacos a partir de datos:*

Mientras que antes era posible establecer la opacidad de la capa, ahora se permite establecer la opacidad para capas de símbolos de forma individual mediante expresiones basadas en un campo de la capa, permitiendo un control más rápido de todos a la misma vez.


*Abrir la galería de estilos desde el Administrador de Estilos:*

Esta novedad ha introducido un nuevo botón que nos permite desde la Interfaz del Administrador de Estilos acceder a la galería oficial de estilos y descargarlos.

Carpeta de recursos [/recursos/03](/recursos/03)

![Open the style gallery from the style manager](/recursos/04/03-1_QGIS_3-18_Abrir_Galeria_Estilos_Adm_Estilos.mp4)

![Data defined overall symbol opacity](/recursos/04/03-2_QGIS_3-18_Opacidad_Simbolos_Desde_Datos.mp4)

### 04 Mesh - Patricio Soriano

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#mesh)

- Feature: New mesh export algorithms
- Feature: Native export for mesh layers
- Feature: Mesh simplification for 3D
- Feature: Multiple native mesh processing algorithms

**Comentarios**

*QGIS sigue incorporando mejoras para el trabajo de datos de tipo malla incluyendo las funcionalidades sin dependencias externas en el mismo core de la aplicación.*

*Desde la Caja de Herramientas de procesamiento podremos usar nuevas herramientas de exportación de caras, bordes o cuadrículas.*

*Si queremos consultar los datos de mallas en una vista 3D podremos definir el nivel de detalle de la renderización que estará directamente relacionado con el rendimiento de la carga de la vista.*

**Recursos**

Carpeta de recursos [/recursos/04](/recursos/04)

![New mesh export algorithms](/recursos/04/New_mesh_export_algorithms.mp4)

![Multiple native mesh processing algorithms](/recursos/04/Multiple_native_mesh_processing_algorithms.mp4)

### 05 Rendering - *No testeada*

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#rendering)

- Feature: “Merged feature” renderer for polygon and line layers
- Feature: Smarter Map Redraws

### 06 3D Features - Emilio Sanchez

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#d-features)

- Feature: Eye dome lighting
- Feature: Data defined 3D material colors
- Feature: 3D Orthographic projection support

**Comentarios**

Para la representación 3D esta versión nos aporta una gran novedad con un gran potencial de representación.

Para el Test, se ha utilizado Windows 10 Home (20H2) y la instalación de 3.18 mediante OSGeo4W en red 64 bits. En un ordenador de sobremesa con Nvidia GTX 1060 6Gb dedicadas y AMD Ryzen 7 2700X de 3.70GHz y 48GB de RAM.
Aporto esta información porque en el modelado 3d, en cualquier software es importante un dimensionado adecuado.
El rendimiento es bastante optimo, y lo he comparado con otro software (Autodesk.Infraworks lic. estudiante) para conseguir el mismo resultado del ejemplo que mostraré.

En el test he cargado un ámbito reducido de la ciudad de Terrassa, en el se representa los usos de los edificios que aporta el cadastro (descargado por el complemento **INSPIRE de QGIS**) y con un trabajo previo de seleccionar los elementos de edificios (*A.ES.SDGC.BU.08279.building.geojson*) y partes del edificio (*A.ES.SDGC.BU.08279.buildingpart.geojson*) del total de la ciudad (no se adjuntan dichos ficheros por ser de libre acceso). El resultado de la selección se guarda en cadastreedif.gpkg (este si en la carpeta de datos) mediante dos capas edipartes y edifurbanos, que se unen por el campo LocalId, que en la tabla de edipartes no exite pero que se puede generar mediante un campo calculado que es : left(LocalId,14), este puede ser un campo virtual o permanente, a elección.

Asignamos una simbología por reglas para cada “currentUse” y guardamos la configuración de de las capas. 

Todos estos procesos no son novedades en 3.18, pero si necesarios, he querido hacer un pequeño recordatorio y apunte de como llegar a tener preparado el proyecto. Toda esta parte no esta incluida en el video que ilustra las novedades.

Ya en el Video podemos ver como se añade las capas citadas y como se hace el Join entre ambas y se prepara la nueva vista de mapa 3d.

De la información de Cadastro sabemos que el campo "numberOfFloorsAboveGround" contiene el número de plantas sobre la rasante, le asignamos un medida para la extrusión de esos valores de 3,3m por planta, obteniendo la representación 3D , y hasta aquí seguimos sin novedades. 

#### Feature: Data defined 3D material colors

Es a partir de este punto que topamos con una gran novedad, la posibilidad de representar la simbología de los objetos 3d, mediante un color basado en valores discriminatorios, que ya pueden ser de los campos asociados o por atributos o propiedades geométricas, por ejemplo. 

En este caso elegí simular la misma representación simbólica de la vista 2D y ver como se podrían aplicar esas reglas a la representación 3D. Así para valor de “currentUse” le asignare el mismo color en valor RGB (que es la forma necesaria) . Y lo hago en esta ocasión en el modo Realistic solo lo valores difuso y Ambiente, pero podría aplicarlo también el especular y si se utiliza el modo CAD podríamos aplicar además del difuso y Especular, también en El cálido y frio. Queda para la siguiente versión la posibilidad de aplicar texturas, que esperamos tenga tan buen resultado como esta novedad.

Para Difuso:

```python
CASE

WHEN left( "edifurbanos_currentUse",1)= '1' THEN '#cc5538' 

WHEN left( "edifurbanos_currentUse",1)= '3' THEN '#CC99CC' 

WHEN left( "edifurbanos_currentUse",3)= '4_1' THEN '#8899DD' 

WHEN left( "edifurbanos_currentUse",3)= '4_2' THEN '#5599BB' 

WHEN left( "edifurbanos_currentUse",3)= '4_3' THEN '#CC8800' 

ELSE '#b3b3b3' END 
```

Para Ambiente:

```python
CASE

WHEN left( "edifurbanos_currentUse",1)= '1' THEN '#9b3f2a' 

WHEN left( "edifurbanos_currentUse",1)= '3' THEN '#826182' 

WHEN left( "edifurbanos_currentUse",3)= '4_1' THEN '#6572a5' 

WHEN left( "edifurbanos_currentUse",3)= '4_2' THEN '#3c6c84' 

WHEN left( "edifurbanos_currentUse",3)= '4_3' THEN '#956300' 

ELSE '#191919' END
```

Como se puede apreciar las opciones son infinitas y da la posibilidad de mediante expresiones (todas ellas) poder llegar a gran numero de opciones.

Una mejora, o quizás otro método seria guardar en una tabla maestra de valores individuales de “currentUse” y los valores rbg que le correspondan. La forma seria de igual manera, pero tan solo hacer indicación del campo que contiene el valor RGB que insisto ha de ser en ese formato… Pero esto ya es a gusto de consumidor y su imaginación.

#### Feature: 3D Orthographic projection suport

Otra novedad es la de poder representar la vista 3D en vista en proyección ortogonal

#### Feature: Eye dome lighting

Este efecto maximiza la sombra según el Angulo de visión de la perspectiva y nos da un “suelo” o base representación de la sombra.

Para ayudar a ilustrar he añadido una Capa WMS con la ortofoto 2500 del ICGC y otra mejora (no aplicada) seria adjuntar un DEM que mejore el relieve final.

**Recursos**

Carpeta de recursos [/recursos/06](/recursos/06)

![3D_Features](/recursos/06/3D_Features.mp4)

### 07 Point Clouds - Damián Ortega

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#point-clouds)

- Feature: Point Cloud Support
- Feature: Add point clouds to browser
- Feature: Untwine PDAL Provider Integration

**Comentarios**

**Recursos**

### 08 Print Layouts - Josep Lluís Sala

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#print-layouts)

- Feature: Gradient ramp based legends
- Feature: Color ramp legend improvements
- Feature: Dynamic text presets

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/06](/recursos/08)

![Gradient ramp based legends](/recursos/08/Gradient_ramp_based_legends.mp4)

![Color ramp legend improvements](/recursos/08/Color_ramp_legend_improvements.mp4)

![Dynamic text presets](/recursos/08/Dynamic_text_presets.mp4)
### 09 Expressions - *No testeada*

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#expressions)

- Feature: Optional formatting of UUID results	
- Feature: Layer CRS variable for expressions	
- Feature: Support for min, max, majority, sum, mean, and median functions on numerical arrays	
- Feature: Negative index for array_get function	
- Feature: Add map_credits function	

### 10 Digitising - Josep Lluís Sala

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#digitising)

- Feature: Select features context menu
- Feature: Curve tracing settings added to UI
- Feature: Feature scaling tool

**Comentarios**

**Recursos**

Carpeta de recursos [/recursos/06](/recursos/10)

![Select features context menu](/recursos/10/Select_features_context_menu.mp4)

![Feature scaling tool](/recursos/10/Feature_scaling_tool.mp4)

### 11 Data Management - David García Hernández	

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#data-management)

- Feature: New export to spreadsheet algorithm
- Feature: Reproject coordinates in the Georeferencer
- Feature: Polymorphic relations/ Document management system	

**Comentarios**

**Recursos**

### 12 Forms and Widgets - *No testeada*

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#forms-and-widgets)

- Feature: Soft and hard constraints in forms

### 13 Analysis Tools - Lucho Ferrer

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#analysis-tools)

- Feature: Nominatim geocoder API

**Comentarios**

**Recursos**

### 14 Processing - *No testeada*

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#processing)

- Feature: Allow expression for order fields in PointsToPath algorithm	
- Feature: Override CRS for Clip Raster by extent output	
- Feature: Add “retain fields” algorithm
- Feature: Reference common field parameter for multiple layers	
- Feature: Extend import geotagged photos to include exif_orientation	
- Feature: Export layer information algorithm	
- Feature: Cell stack percentile and percentrank algorithms	
- Feature: Points to lines processing algorithm	

### 15 Application and Project Options - Emilio Sanchez

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#application-and-project-options)

- Feature: Hidden layers
- Feature: Custom “Full Extent” definition
- Feature: Toggle network caching to QgsNetworkAccessManager
- 
**Comentarios**

Partiendo del proyecto que he utilizado para la presentación de las novedades 3D voy a presentar dos pequeñas novedades de configuración de proyecto muy útiles en casos de gran numero de capas y en aquellos que tenemos capas de cosmética que son muy extensas o al menos más que el ámbito donde queremos estar centrados.

#### Feature: Hidden layers

La capa de Edificios en su parte visual, no nos es útil y por tanto, no es necesario que nos aparezca en el listado de capas. En esta versión podemos ocultarla como se muestra en el vídeo ***(Project>Properties>Private)***

#### Feature: Custom “Full Extent” definition

Otra novedad muy útil para mi y los tipos de proyectos que participo, es el poder fijar un zoom extensión, y resulta fácil y cómodo marcar esta extensión al ámbito de trabajo, ya sea una capa o una caja por coordenadas. En ejemplo podemos ver como el zoom extensión se va al ámbito de la Ortoimagen de toda Cataluña, si configuro al ámbito de la capa de Trabajo, consigo delimitar la vista tanto en 2D como en la 3D. ***(Project>Properties>View Setting>Calculate from Layer)***

**Recursos**

Carpeta de recursos [/recursos/15](/recursos/15)
![Application_and_Project_Options](/recursos/15/Application_and_Project_Options.mp4)

### 16 Browser - Patricio Soriano

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#browser)

- Feature: Unify ArcGis Feature Service and ArcGIS Map Service connections in browser	
- Feature: Allow browsing ArcGIS REST by content groups

**Comentarios**

Desde QGIS podemos conectarnos a servicios de ArcGIS, en concreto a los servicios de entidades (Feature Service) y de mapas (Map Service). Ahora el acceso a ambos se encuentra unificado en el Administrador de fuentes de datos bajo el nombre ArcGIS REST Server. Para diferenciar el tipo de servicios se usa simbología diferente.

**Recursos**

Carpeta de recursos [/recursos/16](/recursos/16)

Ejemplo de ArcGIS REST Sever.

[https://ciudadinteligente.granada.org/arcgis/rest/services/IDE_Publico/2018_CENTRO_NIVELES_PROTECCION_ARQUEOLOGICA/MapServer](https://ciudadinteligente.granada.org/arcgis/rest/services/IDE_Publico/2018_CENTRO_NIVELES_PROTECCION_ARQUEOLOGICA/MapServer)

![Unify ArcGis Feature Service and ArcGIS Map Service connections in browser](/recursos/16/Unify_ArcGis_Feature_Service_and_ArcGIS_Map_Service_connections_in_browser.mp4)
### 17 Data Providers - Patricio Soriano

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#data-providers)

- Feature: Native DXF export algorithm [Bug](https://github.com/qgis/QGIS/issues/41858)
- Feature: Additional geometry types for PostGIS Export
- Feature: Improved network requests with GDAL
- Feature: Read only generated fields
- Feature: Improve MSSQL loading with predefined parameters
- Feature: Filter schemas for MS SQL
- Feature: SAP HANA database support
- Feature: Deprecate support for DB2
- Feature: Oracle connection API
- Feature: Add advanced options for raster data imports

**Comentarios**

*Se ha mejorado las opciones de exportación de datos a formatos CAD de tipo DXF. Usando el geoproceso “Exportar capas a DXF” se podrá seleccionar las capas a exportar e indicar el atributo a partir del cual crear capas CAD.* 

**OJO. Se ha detectado un bug en esta funcionalidad[Bug](https://github.com/qgis/QGIS/issues/41858)**

*Para esta versión, se han añadido todos tipos de geometría disponibles de la operación ogr2ogr de la librería GDAL.En esta mejora, se permite ya definir la dimensión de la geometría a exportar añadiendo la opción 4-dimensional para elementos con valores x, y, z y m.*

*Se incorporan mejoras para las bases de datos Microsoft SQL Server orientadas a optimizar la carga. Esto es posible usando opciones de manejo de extensión de las capas geográficas, la clave primaria o el filtrado por esquema.*

*QGIS ahora es compatible con las bases de datos de SAP HANA, una base de datos en memoria con un motor espacial compatible con OGC*

*Se deja de dar soporte al sistema gestor de base de datos DB2 de IBM.*

*Se han incorporado campos de opciones seleccionables basadas en los parámetros de carga de GDAL según el tipo de archivo. Esto permite definir el total de subprocesos de descompresión, las fuentes de georreferenciación o la omisión de bloques de disco vacíos para archivos de tipo GeoTiff.*

### 18 QGIS Server - *No testeada*

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#qgis-server)

- Feature: GetLegendGraphics Symbol Scale	
- Feature: Drag and drop for WMS GetFeatureInfo response	

### 19 Programmability - *No testeada*

[Enlace a Changelog](https://www.qgis.org/en/site/forusers/visualchangelog318/index.html#programmability)

- Feature: Run multiple items from command history dialog	
- Feature: Enable or disable plugins from the command line	

# Licencia

Esta obra está bajo una licencia Creative Commons Attribution - Share Alike 3.0 - Unported license (CC BY-SA 3.0). El texto de la licencia está disponible en http://creativecommons.org/licenses/by-sa/3.0/ .
